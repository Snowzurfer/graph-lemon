Graphics Programming with Shaders Coursework
======
This software was developed as part of the coursework for the module
_Graphics Programming with Shaders_ at the **University of Abertay Dundee**
in my third year.

### Main Features
* C++ 11
* Use of DirectX 11.2
* Classic pipeline stages
* Geometry stage
* Tessellation stage, tessellation based on distance
* Material-based rendering
* Shadow mapping
* Normal, specular, and alpha mapping
* Model loading from ad-hoc model format

#### Screenshot
![Screenshot software](http://url/screenshot-software.png "screenshot software")

## Installation
1. Clone the repository
2. Download the textures and proprietary model file from [here]()
3. Unzip the textures and model in the main folder of the solution
4. Build and launch from VS2013

## Documentation
Document explaining the software thoroughly [here]()

## Third party libraries
* DirectX 11
* TinyObj
* LodePNG
* STL
* ImGui
* Boost

## Credits
Many, many thanks to my dear friends and classmates Stefano Musumeci, Jiri Klic and Ade Ilori Cross and to my lecturer Paul Robertson.
